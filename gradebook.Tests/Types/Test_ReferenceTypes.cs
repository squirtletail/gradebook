﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gradebook.Tests.Types
{

    [TestClass]
    public class Test_Types
    {

        private void AddGrades(float[] grades)
        {
            grades[1] = 89.5f;
        }

        public void UsingArrays()
        {
            float[] grades;
            grades = new float[5];
            //arrays are reference types
            // so passing it to another function, and making changes to it
            // will make changes to the array we have initalised here

            AddGrades(grades);
            Assert.AreEqual(89.5f, grades[1]);
        }

        [TestMethod]
        public void UppercaseString()
        {
            string name = "anna";
            name = name.ToUpper();
            //name.ToUpper(); -- strings behave like a value type
            // so this will create a new string. It will not change the underlynig
            //name value

            Assert.AreEqual("ANNA", name);
        }

        [TestMethod]
        public void AddDaysToDateTime()
        {
            DateTime date = new DateTime(2018, 03, 27);
            date = date.AddDays(1);

            Assert.AreEqual(28, date.Day);
        }
        private void GiveBookAName(GradeBook book)
        {
            book.Name = "A gradebook.";
        }

        private void IncrementNumber(int num)
        {
            num += 1;
        }

        [TestMethod]
        public void test_ValueTypesPassByValue()
        {
            int x = 46;
            IncrementNumber(x);
            //This takes a COPY of x. The changes are only visible inside the 
            //scope of IncrementNumber.
            //They do not persist outside of that method.

            Assert.AreEqual(46, x);
        }

        [TestMethod]
        public void test_ReferenceTypesPassByValue()
        {
            GradeBook book1 = new GradeBook();
            GradeBook book2 = book1;

            GiveBookAName(book2);
            // The value of book2 is copied into parameter 'book' (a pointer)
            // SO there is a period of time when there are 3 variables pointing
            // to the same memory address
            Assert.AreEqual("A gradebook.", book1.Name);
        }        

        [TestMethod]
        public void test_StringComparisons()
        {
            string name1 = "Anna";
            string name2 = "anna";

            bool result = String.Equals(name1, name2, StringComparison.InvariantCultureIgnoreCase);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void test_IntVariablesHoldValue()
        {
            int x1 = 100;
            int x2 = x1; //Passing the value of x1 into a new memory location.

            x1 = 4;

            Assert.AreNotEqual(x1, x2);
        }

        [TestMethod]
        public void test_GradebookVariablesHoldReference()
        {
            GradeBook g1 = new GradeBook();
            GradeBook g2 = g1; // Setting g2 to point to the same memory address as g1;

            g1.Name = "Anna's gradebook.";

            g1 = new GradeBook(); //Giving g1 a pointer to a new gradebook object.
            g1.Name = "Anna's new gradebook";

            Assert.AreNotEqual(g1.Name, g2.Name);
        }
    }
}
