﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gradebook
{
    class Program
    {
        static void Main(string[] args)
        {
            GradeBook book = new GradeBook();

            book.NameChanged += new NameChangedDelegate(OnNameChanged);

            book.Name = "Anna's grade book";
            book.AddGrade(75);
            book.AddGrade(89.5f);
            book.AddGrade(74);

            GradeStatistics stats = book.ComputeStatistics();
            WriteResult("Average Grade", stats.AverageGrade);
            WriteResult("Lowest Grade: ", stats.LowestGrade);
            WriteResult("Highest Grade: ", (int)stats.HighestGrade);
        }

        static void WriteResult(string desc, float result)
        {
            Console.WriteLine("{0}: {1}", desc, result);
        }

        static void WriteResult(string desc, int result)
        {
            Console.WriteLine(desc + ": " + result);
        }


        static void OnNameChanged(object sender, NameChangedEventArgs args)
        {
            Console.WriteLine($"Grade Book changing name from {args.ExistingName} to {args.NewName}");
        }
    }
}
