﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gradebook
{
    public class GradeBook
    {
        private  List<float> grades;
        private string _name;
        public event NameChangedDelegate NameChanged;

        public GradeBook()
        {
            _name = "Empty";
            grades = new List<float>();
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    if(_name != value)
                    {
                        NameChangedEventArgs args = new NameChangedEventArgs();
                        args.ExistingName = _name;
                        args.NewName = value;

                        NameChanged(this, args);
                    }

                    _name = value;
                }
            }
        }

        public void AddGrade(float grade)
        {
            grades.Add(grade);
        }

        public GradeStatistics ComputeStatistics()
        {
            // Compute highest grade, lowest grade, and average grade

            GradeStatistics stats = new GradeStatistics();
            float sum = 0;

            foreach(float grade in grades)
            {
                stats.HighestGrade = Math.Max(grade, stats.HighestGrade);
                stats.LowestGrade = Math.Min(grade, stats.LowestGrade);
                sum += grade;
            }

            if (grades.Count > 0)
            {
                stats.AverageGrade = sum / grades.Count;
            }

                
            return stats;
        }

    }
}
